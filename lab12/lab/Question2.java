package lab;

public class Question2 {
	
	
	private static int max(int a, int b, int c, int d) {
		int bigger1;
		int bigger2;
		int max;
		bigger1 = (a>b ? a:b);
		bigger2=(c>d ? c:d);
		max = (bigger1>bigger2 ? bigger1:bigger2);
		return max;
	}
	public static void main(String[] args) {
		
		System.out.println(max(12,23,-4,43));
		
	}

}
